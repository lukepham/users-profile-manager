angular.module('app.users.svc', [])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/users/:uuid', {
            templateUrl: 'pages/users/show.html',
            controller: 'ShowUserCtrl'
        });
    }])
    .service('UsersApi', function($http) {
        this.get = function(userId, sucessCallback, failureCallback) {
            $http.get("http://localhost:8080/api/profiles/"+userId)
                .then(function(response) {
                    sucessCallback(response.data)
                }).catch(function (error) {
                if (failureCallback)
                    failureCallback(error);
            });
        }

        this.create = function(userToCreate, successCallback, failureCallback) {
            $http.post("http://localhost:8080/api/profiles", userToCreate)
                .then(function(response) {
                    console.log("User created at Api: " + JSON.stringify(response.data))
                    successCallback(response.data);
                }).catch(function(error) {
                if (failureCallback)
                    failureCallback(error);
            })
        }

        this.update = function(userId, userToUpdate, sucessCallback, failureCallback) {
            $http.patch("http://localhost:8080/api/profiles/"+userId, userToUpdate)
                .then(function(response) {
                    sucessCallback(response.data)
                }).catch(function(error) {
                if (failureCallback)
                    failureCallback(error);
            })
        };

        this.delete = function(userId, sucessCallback, failureCallback) {
            $http.delete("http://localhost:8080/api/profiles/"+userId)
                .then(function(response) {
                    sucessCallback(response.data)
                }).catch(function(error) {
                if (failureCallback)
                    failureCallback(error);
            })
        };
    })
