angular.module('app.users.ctrl', ['ngRoute', 'app.users.svc'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when(ROUTES.NEW, {
            templateUrl: 'pages/users/new.html',
            controller: 'NewUserCtrl',
            authorize: true
        });

        $routeProvider.when(ROUTES.SHOW, {
            templateUrl: 'pages/users/show.html',
            controller: 'ShowUserCtrl',
            authorize: true
        });

        $routeProvider.when(ROUTES.EDIT, {
            templateUrl: 'pages/users/edit.html',
            controller: 'EditUserCtrl',
            authorize: true
        });
    }])
    .controller('NewUserCtrl', ['$scope', '$location', 'UsersApi', function($scope, $location, UsersApi) {
        $scope.create = function() {
            UsersApi.create($scope.user, function(userCreated) {
                $location.path(ROUTES.buildShowUrl(userCreated.id));
            })
        }
    }])
    .controller('ShowUserCtrl', ['$scope', '$routeParams', '$location', 'UsersApi', function($scope, $routeParams, $location, UsersApi) {
        $scope.$on('$viewContentLoaded', function () {
            if ($routeParams.uuid === "" || $routeParams.uuid === 'undefined')
                $location.path('/error');

            UsersApi.get($routeParams.uuid, function (user) {
                $scope.user = user;
            });
        });
    }])
    .controller('EditUserCtrl', ['$scope', '$routeParams', '$location', 'UsersApi', function($scope, $routeParams, $location, UsersApi) {
        $scope.$on('$viewContentLoaded', function () {
            if ($routeParams.uuid === "" || $routeParams.uuid === 'undefined')
                $location.path('/error');

            UsersApi.get($routeParams.uuid, function (user) {
                $scope.user = user;
            });
        });

        $scope.update = function() {
            UsersApi.update($routeParams.uuid, $scope.user, function(userUpdated) {
                $location.path(ROUTES.buildShowUrl(userUpdated.id));
            })
        }

        $scope.delete = function() {
            UsersApi.delete($routeParams.uuid, function(userDeleted) {
                $location.path(ROUTES.SEARCH);
            })
        }
    }]);

var ROUTES = {
    NEW: '/new-user',
    EDIT: '/edit-user/:uuid',
    SHOW: '/show-user/:uuid',
    SEARCH: '/search-users',
    buildEditUrl: function(uuid) {
        return '/edit-user/' + uuid;
    },
    buildShowUrl: function(uuid) {
        return '/show-user/' + uuid;
    }
}
