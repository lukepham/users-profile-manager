angular.module('app.search.ctrl', ['ngRoute', 'app.search.svc', 'app.auth.svc'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/search-users', {
            templateUrl: 'pages/search/search.html',
            controller: 'SearchUserCtrl',
            authorize: true
        });
    }])
    .controller('SearchUserCtrl', ['$scope', 'SearchUserApi', function($scope, SearchUserApi) {
        $scope.$on('$viewContentLoaded', function(){
            SearchUserApi.all(function(data) {
                $scope.users = data.content;
                $scope.page = data.number;
                $scope.total = data.totalElements;
            });
        });

        $scope.query = function(newPage) {
            SearchUserApi.query($scope.query.term, newPage, function(data) {
                $scope.users = data.content;
                $scope.page = data.number;
                $scope.total = data.totalElements;
            });
        }
    }]);
