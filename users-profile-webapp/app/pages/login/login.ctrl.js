angular.module('app.login.ctrl', ['ngRoute', 'app.login.svc', 'app.auth.svc'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/signin', {
            templateUrl: 'pages/login/login.html',
            controller: 'LoginCtrl'
        })
    }])
    .controller('LoginCtrl', ['$scope', '$location', '$window', 'LoginApi', 'TokenManager', function($scope, $location, $window, LoginApi, TokenManager) {
        $scope.isLoggedIn = TokenManager.hasToken();
        $scope.hasLoginError = false;

        $scope.signin = function() {
            $scope.hasLoginError = false;
            LoginApi.signin($scope.authentication,
                function(token) {
                    $scope.hasLoginError = false;
                    TokenManager.setToken(token);
                }, function(token) {
                    $scope.hasLoginError = false;
                    $window.location.reload();
                    window.location = "#/search-users"
                }, function(error) {
                    $scope.hasLoginError = true;
                })
        }

        $scope.signout = function(auth) {
            TokenManager.removeToken();

            $window.location.reload();
            $location.path('/signin');
        }

    }]);
