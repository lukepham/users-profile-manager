'use strict';

// Declare app level module which depends on views, and components
angular.module('app', [
    'ngRoute',
    'app.login.ctrl',
    'app.search.ctrl',
    'app.users.ctrl'
]).config(['$locationProvider', '$routeProvider', '$httpProvider', function ($locationProvider, $routeProvider, $httpProvider) {
    $routeProvider.otherwise({redirectTo: '/signin'});
}]);

angular.module('app.auth.svc', [])
    .factory('TokenManager', ['$window', function($window, $q) {
        return {
            getToken: function() {
                return $window.localStorage['jwtToken'];
            },
            setToken: function(token) {
                $window.localStorage['jwtToken'] = token;
            },
            removeToken: function() {
                delete $window.localStorage['jwtToken'];
            },
            hasToken: function() {
                return $window.localStorage['jwtToken'] !== '' && $window.localStorage['jwtToken'] !== undefined
            }
        }
    }]).factory('AuthInterceptor', function($location, TokenManager, $q) {
        return {
            request: function(config) {
                config.headers = config.headers || {};

                if (TokenManager.getToken()) {
                    config.headers['Authorization'] = 'Bearer ' + TokenManager.getToken();
                }

                return config;
            },

            responseError: function(response) {
                console.log(JSON.stringify(response))
                if (response.status === 401 || response.status === 403) {
                    $location.path('/signin');
                }

                if (response.status === 500) {
                    $location.path('/500');
                }

                return $q.reject(response);
            }
        }
    }).config(function($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    }).run(function ($rootScope, $location, TokenManager) {
        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            if (next.authorize) {
                if (!TokenManager.getToken()) {
                    event.preventDefault();
                    $location.path('/signin');
                }
            }
        });
    });
