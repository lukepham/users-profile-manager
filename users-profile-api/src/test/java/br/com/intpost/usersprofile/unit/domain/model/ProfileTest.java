package br.com.intpost.usersprofile.unit.domain.model;

import br.com.intpost.usersprofile.domain.model.Profile;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ProfileTest {

    @BeforeClass
    public static void setUpAll() {
        FixtureFactoryLoader.loadTemplates("br.com.intpost.usersprofile.fixture");
    }

    @Test
    public void builder() {
        final Profile profileOne = Profile.builder()
            .id(1l)
            .position("sf1")
            .username("us1")
            .email("email1@example.com")
            .firstName("fs1")
            .lastName("ls1")
            .postalCode("050")
            .about("im sf1")
        .build();

        final Profile profileOneEqual = Profile.builder()
            .id(1l)
            .position("sf1")
            .username("us1")
            .email("email1@example.com")
            .firstName("fs1")
            .lastName("ls1")
            .postalCode("050")
            .about("im sf1")
            .build();

        final Profile profileTwo = Profile.builder()
            .id(2l)
            .position("sf2")
            .username("us2")
            .email("email2@example.com")
            .firstName("fs2")
            .lastName("ls2")
            .postalCode("052")
            .about("im sf2")
        .build();

        assertThat(profileOne).isNotEqualTo(profileTwo);
        assertThat(profileOne).isEqualTo(profileOneEqual);

        assertThat(profileOne).isEqualTo(profileOneEqual);
        assertThat(profileOne).isNotSameAs(profileOneEqual);

        assertThat(profileOne.getId()).isEqualTo(1);
        assertThat(profileOne.getId()).isEqualTo(1l);
        assertThat(profileOne.getPosition()).isEqualTo("sf1");
        assertThat(profileOne.getUsername()).isEqualTo("us1");
        assertThat(profileOne.getEmail()).isEqualTo("email1@example.com");
        assertThat(profileOne.getFirstName()).isEqualTo("fs1");
        assertThat(profileOne.getLastName()).isEqualTo("ls1");
        assertThat(profileOne.getPostalCode()).isEqualTo("050");
        assertThat(profileOne.getAbout()).isEqualTo("im sf1");
    }

}