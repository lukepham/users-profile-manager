package br.com.intpost.usersprofile.fixture;

import br.com.intpost.usersprofile.domain.model.Profile;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

public class ProfileFixture implements TemplateLoader {

	@Override
	public void load() {
		Fixture.of(Profile.class).addTemplate("new", new Rule(){{
		    add("position", "Software Engineer");
		    add("email", "software@example.com");
		    add("username", "gprado");
			add("password", "1234");
			add("firstName", "Gabriel");
			add("lastName", "Prado");
			add("postalCode", "05023030");
			add("about", "I'm a software engineer...");
			add("roles", "ADMIN");
            add("image", "asdfasdfa");
		}});
		
		Fixture.of(Profile.class).addTemplate("valid").inherits("new", new Rule(){{
		    add("id", random(Long.class, range(1L, 1000L)));
		}});		
	}
	
}
