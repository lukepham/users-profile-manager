package br.com.intpost.usersprofile.integration.domain.repository;

import br.com.intpost.usersprofile.config.TestingDatabaseContext;
import br.com.intpost.usersprofile.domain.model.Profile;
import br.com.intpost.usersprofile.domain.repository.ProfileDatabaseRepository;
import br.com.intpost.usersprofile.util.MergeBeans;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestingDatabaseContext.class, initializers = ConfigFileApplicationContextInitializer.class)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD) // for isolated/clean database tests 
public class ProfileRepositoyTest {

    @Autowired
    private ProfileDatabaseRepository repository;

    @BeforeClass
    public static void setUpAll() {
        FixtureFactoryLoader.loadTemplates("br.com.intpost.usersprofile.fixture");
    }

    @Before
    public void setUp() {
        prepareDatabase();
    }

    @Test
    public void findAll() {
        final List<Profile> profiles = repository.findAll();
        assertThat(profiles.size()).isEqualTo(2);
    }

    @Test
    public void save() {
        final Profile profile = Fixture.from(Profile.class).gimme("new");
        final Profile profileSaved = repository.save(profile);

        assertThat(profileSaved.getId()).isNotNull();
        assertThat(profileSaved.getPosition()).isEqualTo(profile.getPosition());
        assertThat(profileSaved.getUsername()).isEqualTo(profile.getUsername());
        assertThat(profileSaved.getEmail()).isEqualTo(profile.getEmail());
        assertThat(profileSaved.getFirstName()).isEqualTo(profile.getFirstName());
        assertThat(profileSaved.getLastName()).isEqualTo(profile.getLastName());
        assertThat(profileSaved.getPostalCode()).isEqualTo(profile.getPostalCode());
        assertThat(profileSaved.getAbout()).isEqualTo(profile.getAbout());
    }

    @Test
    public void update() throws InvocationTargetException, IllegalAccessException {
        final Profile profileSaved = repository.findOne(1l);

        final Profile profile = Profile
            .builder()
            .id(profileSaved.getId())
            .username("gprado-edited")
        .build();

        new MergeBeans().copyProperties(profileSaved, profile);

        final Profile profileUpdated = repository.save(profileSaved);
        assertThat(profileSaved.getId()).isEqualTo(profileUpdated.getId());
        assertThat(profileSaved.getPosition()).isEqualTo(profileUpdated.getPosition());
        assertThat(profileSaved.getUsername()).isEqualTo("gprado-edited");
        assertThat(profileSaved.getEmail()).isNotNull();
    }

    private void prepareDatabase() {
        final Profile profileOne = Fixture.from(Profile.class).gimme("new");
        final Profile profileTwo = Fixture.from(Profile.class).gimme("new");
        final Profile profileOneSaved = repository.save(profileOne);
        final Profile profileTwoSaved = repository.save(profileTwo);

        assertThat(profileOne).isEqualTo(profileOneSaved);
        assertThat(profileTwo).isEqualTo(profileTwoSaved);
    }

}
