package br.com.intpost.usersprofile.controller;

import br.com.intpost.usersprofile.domain.model.Profile;
import br.com.intpost.usersprofile.domain.repository.ProfileDatabaseRepository;
import br.com.intpost.usersprofile.domain.service.ProfileSearchService;
import br.com.intpost.usersprofile.domain.service.ProfileService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Log
@RequestMapping("/api/profiles")
@RestController()
public class ProfileController {

    @Autowired
    private ProfileDatabaseRepository repository;
    @Autowired
    private ProfileService service;
    @Autowired
    private ProfileSearchService searchService;

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Profile> search(@RequestParam(value="q", defaultValue = "") final String queryTerm, @RequestParam(value="page", defaultValue="0") final int page) {
        log.info(String.format("Searching for profiles using queryTerm=%s in page=%d", queryTerm, page));

        return searchService.search(queryTerm, page);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Profile> create(@Valid @RequestBody final Profile profileForm) throws Exception {
        log.info(String.format("Creating profile=%s", profileForm));

        // To be Immutable use ProfileForm or another builder helper that supports copy properties from reference object
        final Profile profileToCreate = Profile.builder()
                .position(profileForm.getPosition())
                .username(profileForm.getUsername())
                .password(profileForm.getPassword())
                .email(profileForm.getEmail())
                .firstName(profileForm.getFirstName())
                .lastName(profileForm.getLastName())
                .postalCode(profileForm.getPostalCode())
                .about(profileForm.getAbout())
                .build();

        final Profile profileSaved = service.create(profileToCreate);

        return new ResponseEntity<>(profileSaved, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Profile> show(@PathVariable("id") final Long id) throws Exception {
        log.info(String.format("Show profile with id=%d", id));

        final Profile Profile = service.findOne(id);

        return new ResponseEntity<>(Profile, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public Profile update(@PathVariable final Long id, @Valid @RequestBody final Profile profileForm) throws Exception {
        log.info(String.format("Update profile=%s with id=%d...", profileForm, id));

        // To be Immutable use ProfileForm or another builder helper that supports copy properties from reference object
        final Profile profileToUpdate = Profile.builder()
                .id(id)
                .position(profileForm.getPosition())
                .username(profileForm.getUsername())
                .password(profileForm.getPassword())
                .email(profileForm.getEmail())
                .firstName(profileForm.getFirstName())
                .lastName(profileForm.getLastName())
                .postalCode(profileForm.getPostalCode())
                .about(profileForm.getAbout())
                .build();

        return service.update(profileToUpdate);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void destroy(@PathVariable("id") final Long id) throws Exception {
        service.delete(id);
    }
}
