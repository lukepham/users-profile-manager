package br.com.intpost.usersprofile.controller.filters;

import br.com.intpost.usersprofile.controller.helper.TokenAuthenticationHelper;
import lombok.extern.java.Log;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Log
public class JWTAuthenticationFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException {
        final Authentication authentication = TokenAuthenticationHelper
                .getAuthentication((HttpServletRequest) request);

        SecurityContextHolder
                .getContext()
                .setAuthentication(authentication);

        log.info(String.format("Authenticating %s...", authentication));

        filterChain.doFilter(request, response);
    }

}
