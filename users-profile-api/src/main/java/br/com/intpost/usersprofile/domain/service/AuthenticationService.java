package br.com.intpost.usersprofile.domain.service;

import br.com.intpost.usersprofile.domain.model.Profile;
import br.com.intpost.usersprofile.domain.repository.ProfileDatabaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;

@Service
public class AuthenticationService {

    @Autowired
    private ProfileDatabaseRepository profileDatabaseRepository;

    @Cacheable(cacheNames="auth", key="#username")
    public Optional<User> findByUsername(final String username) {
        final Profile authProfile = profileDatabaseRepository.findByUsername(username);

        if (authProfile == null)
            return Optional.empty();

        final User user = new User(
            authProfile.getUsername(),
            authProfile.getPassword(),
            Arrays.asList(new SimpleGrantedAuthority(authProfile.getRoles()))
        );

        return Optional.ofNullable(user);
    }
}
