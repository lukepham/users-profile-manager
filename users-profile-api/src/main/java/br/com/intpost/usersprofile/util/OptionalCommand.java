package br.com.intpost.usersprofile.util;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class OptionalCommand<T> {

    static public <T> OptionalCommand<T> from(final Optional<T> optional) {
        return new OptionalCommand<T>(optional);
    }

    private final Optional<T> optional;
    private OptionalCommand(final Optional<T> optional) {
        this.optional = optional;
    }

    public <U> T ifPresent(final Function<? super T, T> mapper) throws Exception {
        return optional
                .map(c -> mapper.apply(c))
                .orElseThrow(() -> new Exception("Resource not found."));
    }

    public T okIfPresent(final Consumer<? super T> consumer) throws Exception {
        return optional
                .map(c -> {
                    consumer.accept(c);
                    return c;
                })
                .orElseThrow(() -> new Exception("Resource not found."));
    }
}