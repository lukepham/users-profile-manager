package br.com.intpost.usersprofile.domain.service;

import br.com.intpost.usersprofile.domain.model.Profile;
import br.com.intpost.usersprofile.domain.repository.ProfileDatabaseRepository;
import liquibase.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ProfileSearchService {

    @Autowired
    private ProfileDatabaseRepository profileDatabaseRepository;

    public Page<Profile> search(final String queryTerm, final int page) {
        if (StringUtils.isEmpty(queryTerm)) {
            return profileDatabaseRepository.findAll(new PageRequest(page, 10));
        }

        final Page<Profile> pagedProfiles = profileDatabaseRepository
            .findByFirstNameContainingIgnoreCase(
                queryTerm,
                new PageRequest(page, 10));
        return pagedProfiles;
    }

}
