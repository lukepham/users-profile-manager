package br.com.intpost.usersprofile.domain.service;

import br.com.intpost.usersprofile.domain.model.Profile;
import br.com.intpost.usersprofile.domain.repository.ProfileDatabaseRepository;
import br.com.intpost.usersprofile.util.MergeBeans;
import br.com.intpost.usersprofile.util.OptionalCommand;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

@Log
@Service
public class ProfileService {

    @Autowired
    private ProfileDatabaseRepository profileDatabaseRepository;

    @Cacheable(cacheNames="profile", key="#id")
    public Profile findOne(final Long id) throws Exception {
        final Profile profile = Optional.ofNullable(profileDatabaseRepository.findOne(id))
            .orElseThrow(() -> new Exception(String.format("Profile with id=%d not found.", id)));

        log.info(String.format("Profile=%s found at database", profile));

        return profile;
    }

    @Caching(put = {
        @CachePut(cacheNames="profile", key="#profile.id"),
        @CachePut(cacheNames="auth", key="#profile.username")})
    public Profile create(final Profile profile) {
        profile.setRoles("USER");
        
        final Profile profileSaved = profileDatabaseRepository.save(profile);
        log.info(String.format("Profile created=%s", profileSaved));

        return profileSaved;
    }

    @Caching(put = {
        @CachePut(cacheNames="profile", key="#profile.id"),
        @CachePut(cacheNames="auth", key="#profile.username")})
    public Profile update(final Profile profile) throws Exception {

        final Profile profileUpdated = OptionalCommand
            .from(Optional.ofNullable(profileDatabaseRepository.findOne(profile.getId())))
            .ifPresent(p -> {
                log.info(String.format("Profile=%d found=%s", p.getId(), p));

                try {
                    new MergeBeans().copyProperties(p, profile);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }

                log.info(String.format("Profile=%d merged=%s", p.getId(), p));

                return profileDatabaseRepository.save(p);
            });

        log.info(String.format("Profile updated=%s", profileUpdated));

        return profileUpdated;
    }

    @CacheEvict(cacheNames="profile", key="#id")
    public void delete(final Long id) throws Exception {
        OptionalCommand.from(Optional.ofNullable(profileDatabaseRepository.findOne(id))).ifPresent(p -> {
            profileDatabaseRepository.delete(p.getId());
            log.info(String.format("Profile deleted=%s", p));
            return p;
        });
    }

}
